/**
 * This code is part of the lab exercises for the Compilers course at Harokopio
 * University of Athens, Dept. of Informatics and Telematics.
 */
package org.hua.ast;

public class CompilationUnit extends ASTNode {

    private ClassDefinition myClass;
    private FunctionDefinition myFunction;


    public CompilationUnit() {
    }
    public CompilationUnit(ClassDefinition Class) {
        this.myClass = Class;
    }
    
      public CompilationUnit(FunctionDefinition Function) {
        this.myFunction = Function;
    }

    public ClassDefinition getMyClass() {
        return myClass;
    }

    public void setMyClass(ClassDefinition myClass) {
        this.myClass = myClass;
    }

    public FunctionDefinition getMyFunction() {
        return myFunction;
    }

    public void setMyFunction(FunctionDefinition myFunction) {
        this.myFunction = myFunction;
    }


    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
