/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

import java.util.List;
import org.objectweb.asm.Type;

/**
 *
 * @author Tilemachos
 */
public class FunctionDefinition extends ASTNode {

    private Type ts;
    private String id;
    private CompoundStatement cs;
    private List<ParameterDeclaration> parameters;
    private FunctionDefinition fd;
    private ClassDefinition cd;

    public FunctionDefinition(Type ts, String id, CompoundStatement cs) {
        this.ts = ts;
        this.id = id;
        this.cs = cs;
    }

    public FunctionDefinition(Type ts, String id, CompoundStatement cs, List<ParameterDeclaration> parameters) {
        this.ts = ts;
        this.id = id;
        this.cs = cs;
        this.parameters = parameters;
    }

    public FunctionDefinition(Type ts, String id, CompoundStatement cs, FunctionDefinition fd) {
        this.ts = ts;
        this.id = id;
        this.cs = cs;
        this.fd = fd;
    }

    public FunctionDefinition(Type ts, String id, CompoundStatement cs, List<ParameterDeclaration> parameters, FunctionDefinition fd) {
        this.ts = ts;
        this.id = id;
        this.cs = cs;
        this.parameters = parameters;
        this.fd = fd;
    }

    public FunctionDefinition(Type ts, String id, CompoundStatement cs, ClassDefinition cd) {
        this.ts = ts;
        this.id = id;
        this.cs = cs;
        this.cd = cd;
    }

    public FunctionDefinition(Type ts, String id, CompoundStatement cs, List<ParameterDeclaration> parameters, ClassDefinition cd) {
        this.ts = ts;
        this.id = id;
        this.cs = cs;
        this.parameters = parameters;
        this.cd = cd;
    }

    public FunctionDefinition(FunctionDefinition fd) {
        this.fd = fd;
    }

    public Type getTs() {
        return ts;
    }

    public void setTs(Type ts) {
        this.ts = ts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CompoundStatement getCs() {
        return cs;
    }

    public void setCs(CompoundStatement cs) {
        this.cs = cs;
    }

    public List<ParameterDeclaration> getParameters() {
        return parameters;
    }

    public void setPl(List<ParameterDeclaration> parameters) {
        this.parameters = parameters;
    }

    public FunctionDefinition getFd() {
        return fd;
    }

    public void setFd(FunctionDefinition fd) {
        this.fd = fd;
    }

    public ClassDefinition getCd() {
        return cd;
    }

    public void setCd(ClassDefinition cd) {
        this.cd = cd;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
