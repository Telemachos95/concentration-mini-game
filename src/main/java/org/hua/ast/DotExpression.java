/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

import java.util.List;

public class DotExpression extends Expression {

    private String identifier;
    private Expression expression;
    private List<Expression> expressions;

    public DotExpression(String identifier, Expression expression){
        this.identifier = identifier;
        this.expression = expression;
    }
    public DotExpression(String identifier, Expression expression, List<Expression> expressions){
        this.identifier  = identifier;
        this.expression  = expression;
        this.expressions = expressions;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public List<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<Expression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }
    
}