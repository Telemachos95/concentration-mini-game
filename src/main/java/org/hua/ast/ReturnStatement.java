/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

/**
 *
 * @author Tilemachos
 */
public class ReturnStatement extends Statement {
    
    private Expression e;
    
    public ReturnStatement() {
    }

    public ReturnStatement(Expression e) {
        this.e = e;
    }

    public Expression getE() {
        return e;
    }

    public void setE(Expression e) {
        this.e = e;
    }
    
    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }
    
}
