/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

import org.objectweb.asm.Type;

/**
 *
 * @author Tilemachos
 */
public class FieldDefinition extends ASTNode {

    private Type ts;
    private String id;
    private FieldDefinition fd;
    private FunctionDefinition funcDef;

    public FieldDefinition(Type ts, String id) {
        this.ts = ts;
        this.id = id;
    }

    public FunctionDefinition getFuncDef() {
        return funcDef;
    }

    public void setFuncDef(FunctionDefinition funcDef) {
        this.funcDef = funcDef;
    }

    public FieldDefinition(Type ts, String id, FunctionDefinition funcDef) {
        this.ts = ts;
        this.id = id;
        this.funcDef = funcDef;
    }

    public FieldDefinition(Type ts, String id, FieldDefinition fd) {
        this.ts = ts;
        this.id = id;
        this.fd = fd;
    }

    public Type getTs() {
        return ts;
    }

    public void setTs(Type ts) {
        this.ts = ts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FieldDefinition getFd() {
        return fd;
    }

    public void setFd(FieldDefinition fd) {
        this.fd = fd;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
