/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

import org.objectweb.asm.Type;

/**
 *
 * @author Tilemachos
 */
public class ParameterDeclaration extends ASTNode {

    private Type ts;
    private String id;

    public ParameterDeclaration(Type ts, String id) {
        this.ts = ts;
        this.id = id;
    }

    public Type getTs() {
        return ts;
    }

    public void setTs(Type ts) {
        this.ts = ts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }
}
