/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

/**
 *
 * @author Tilemachos
 */
public class VoidLiteralExpression extends Expression {

    private Void literal;

    public VoidLiteralExpression(Void literal) {
        this.literal = literal;
    }

    public Void getLiteral() {
        return literal;
    }

    public void setLiteral(Void literal) {
        this.literal = literal;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
