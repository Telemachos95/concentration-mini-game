/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

/**
 *
 * @author Tilemachos
 */
public class ClassDefinition extends ASTNode {

    private String identifier;
    private FieldOrFunctionDefinition fofd;
    private ClassDefinition myClass;
    private FunctionDefinition myFunction;

    public ClassDefinition(String identifier) {
        this.identifier = identifier;
    }

    public ClassDefinition(String identifier, FieldOrFunctionDefinition fofd) {
        this.identifier = identifier;
        this.fofd = fofd;
    }

    public ClassDefinition(String identifier, ClassDefinition myClass) {
        this.identifier = identifier;
        this.myClass = myClass;
    }

    public ClassDefinition(String identifier, FunctionDefinition myFunction) {
        this.identifier = identifier;
        this.myFunction = myFunction;
    }

    public ClassDefinition(String identifier, FieldOrFunctionDefinition fofd, ClassDefinition myClass) {
        this.identifier = identifier;
        this.fofd = fofd;
        this.myClass = myClass;
    }

    public ClassDefinition(String identifier, FieldOrFunctionDefinition fofd, FunctionDefinition myFunction) {
        this.identifier = identifier;
        this.fofd = fofd;
        this.myFunction = myFunction;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public FieldOrFunctionDefinition getFofd() {
        return fofd;
    }

    public void setFofd(FieldOrFunctionDefinition fofd) {
        this.fofd = fofd;
    }

    public ClassDefinition getMyClass() {
        return myClass;
    }

    public void setMyClass(ClassDefinition myClass) {
        this.myClass = myClass;
    }

    public FunctionDefinition getMyFunction() {
        return myFunction;
    }

    public void setMyFunction(FunctionDefinition myFunction) {
        this.myFunction = myFunction;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
