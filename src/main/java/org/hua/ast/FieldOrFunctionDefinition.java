/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hua.ast;

/**
 *
 * @author Tilemachos
 */
public class FieldOrFunctionDefinition extends ASTNode {
    
    private FieldDefinition field;
    private FunctionDefinition myFunction;

    public FieldOrFunctionDefinition(FunctionDefinition myFunction) {
        this.myFunction = myFunction;
    }

    public FieldOrFunctionDefinition(FieldDefinition field) {
        this.field = field;
    }

    public FieldDefinition getField() {
        return field;
    }

    public void setField(FieldDefinition field) {
        this.field = field;
    }

    public FunctionDefinition getMyFunction() {
        return myFunction;
    }

    public void setMyFunction(FunctionDefinition myFunction) {
        this.myFunction = myFunction;
    }
    
    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
         visitor.visit(this);
    }
    
}
