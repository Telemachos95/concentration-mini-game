package org.hua;

/**
 * This code is part of the lab exercises for the Compilers course at Harokopio
 * University of Athens, Dept. of Informatics and Telematics.
 */
import java.util.ArrayDeque;
import java.util.Deque;

import org.hua.ast.ASTUtils;
import org.hua.ast.ASTVisitor;
import org.hua.ast.ASTVisitorException;
import org.hua.ast.AssignmentStatement;
import org.hua.ast.BinaryExpression;
import org.hua.ast.BreakStatement;
import org.hua.ast.ClassDefinition;
import org.hua.ast.CompilationUnit;
import org.hua.ast.CompoundStatement;
import org.hua.ast.ContinueStatement;
import org.hua.ast.DeclarationStatement;
import org.hua.ast.DotExpression;
import org.hua.ast.DoubleLiteralExpression;
import org.hua.ast.Expression;
import org.hua.ast.FieldDefinition;
import org.hua.ast.FieldOrFunctionDefinition;
import org.hua.ast.FunctionDefinition;
import org.hua.ast.IdentifierExpression;
import org.hua.ast.IdentifierExpressionList;
import org.hua.ast.IfElseStatement;
import org.hua.ast.IfStatement;
import org.hua.ast.IntegerLiteralExpression;
import org.hua.ast.LogicalNotExpression;
import org.hua.ast.NewExpression;
import org.hua.ast.NullExpression;
import org.hua.ast.ParameterDeclaration;
import org.hua.ast.ParenthesisExpression;
import org.hua.ast.PrintStatement;
import org.hua.ast.ReturnStatement;
import org.hua.ast.Statement;
import org.hua.ast.StringLiteralExpression;
import org.hua.ast.ThisExpression;
import org.hua.ast.UnaryExpression;
import org.hua.ast.VariousExpressions;
import org.hua.ast.VoidLiteralExpression;
import org.hua.ast.WhileStatement;
import org.hua.symbol.LocalIndexPool;

/**
 * Build LocalIndexPool for each node of the AST.
 */
public class LocalIndexBuilderASTVisitor implements ASTVisitor {

    private final Deque<LocalIndexPool> env;

    public LocalIndexBuilderASTVisitor() {
        env = new ArrayDeque<LocalIndexPool>();
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression1() != null) {
            node.getExpression1().accept(this);
        }
        if (node.getExpression2() != null) {
            node.getExpression2().accept(this);
        }
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression1().accept(this);
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(UnaryExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
        if (node.getStatement() != null) {
            node.getStatement().accept(this);
        }
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getStatements() != null) {
            for (Statement s : node.getStatements()) {
                s.accept(this);
            }
        }

        if (node.getCs() != null) {
            node.getCs().accept(this);
        }
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
        node.getStatement1().accept(this);
        node.getStatement2().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

        if (node.getStatement() != null) {
            node.getStatement().accept(this);
        }
    }

    @Override
    public void visit(CompilationUnit node) throws ASTVisitorException {
        env.push(new LocalIndexPool());
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
        env.pop();
    }

    @Override
    public void visit(ClassDefinition node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getFofd() != null) {
            node.getFofd().accept(this);
        }

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
    }

    @Override
    public void visit(FieldOrFunctionDefinition node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getField() != null) {
            node.getField().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
    }

    @Override
    public void visit(FieldDefinition node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getFd() != null) {
            node.getFd().accept(this);
        }
        if (node.getFuncDef() != null) {
            node.getFuncDef().accept(this);
        }
    }

    @Override
    public void visit(FunctionDefinition node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getParameters() != null) {
            for (ParameterDeclaration pd : node.getParameters()) {
                pd.accept(this);
            }
        }

        if (node.getCs()
                != null) {
            node.getCs().accept(this);
        }

        if (node.getCd()
                != null) {
            node.getCd().accept(this);
        }

        if (node.getFd()
                != null) {
            node.getFd().accept(this);
        }
    }

    @Override
    public void visit(ParameterDeclaration node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(VoidLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    @Override
    public void visit(LogicalNotExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(DotExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

        if (node.getExpressions() != null) {
            if (!node.getExpressions().isEmpty()) {
                for (Expression e : node.getExpressions()) {
                    e.accept(this);
                }
            }
        }
    }

    @Override
    public void visit(NewExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        for (Expression e : node.getExpressions()) {
            e.accept(this);
        }
    }

    @Override
    public void visit(IdentifierExpressionList node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpressions() != null) {
            if (!node.getExpressions().isEmpty()) {
                for (Expression e : node.getExpressions()) {
                    e.accept(this);
                }
            }
        }
    }

    @Override
    public void visit(ThisExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(NullExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(DeclarationStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
    }

    @Override
    public void visit(VariousExpressions node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    @Override
    public void visit(PrintStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
    }

}
