/**
 * This code is part of the lab exercises for the Compilers course at Harokopio
 * University of Athens, Dept. of Informatics and Telematics.
 */
package org.hua;

import org.hua.symbol.SymTable;
import org.hua.symbol.SymTableEntry;
import org.hua.ast.ASTUtils;
import org.hua.ast.ASTVisitor;
import org.hua.ast.ASTVisitorException;
import org.hua.ast.AssignmentStatement;
import org.hua.ast.BinaryExpression;
import org.hua.ast.BreakStatement;
import org.hua.ast.ClassDefinition;
import org.hua.ast.CompilationUnit;
import org.hua.ast.CompoundStatement;
import org.hua.ast.ContinueStatement;
import org.hua.ast.DoubleLiteralExpression;
import org.hua.ast.IdentifierExpression;
import org.hua.ast.IfElseStatement;
import org.hua.ast.IfStatement;
import org.hua.ast.IntegerLiteralExpression;
import org.hua.ast.ParenthesisExpression;
import org.hua.ast.StringLiteralExpression;
import org.hua.types.TypeUtils;
import org.hua.ast.UnaryExpression;
import org.hua.ast.DeclarationStatement;
import org.hua.ast.DotExpression;
import org.hua.ast.Expression;
import org.hua.ast.FieldDefinition;
import org.hua.ast.FieldOrFunctionDefinition;
import org.hua.ast.FunctionDefinition;
import org.hua.ast.IdentifierExpressionList;
import org.hua.ast.LogicalNotExpression;
import org.hua.ast.NewExpression;
import org.hua.ast.NullExpression;
import org.hua.ast.ParameterDeclaration;
import org.hua.ast.PrintStatement;
import org.hua.ast.ReturnStatement;
import org.hua.ast.Statement;
import org.hua.ast.ThisExpression;
import org.hua.ast.VariousExpressions;
import org.hua.ast.VoidLiteralExpression;
import org.hua.ast.WhileStatement;
import org.objectweb.asm.Type;
import org.hua.types.TypeException;

/**
 * Compute possible types for each node.
 */
public class CollectTypesASTVisitor implements ASTVisitor {

    public CollectTypesASTVisitor() {
    }

    @Override
    public void visit(CompilationUnit node) throws ASTVisitorException {

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }

        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookupOnlyInTop("function_main") == null) {
            ASTUtils.error(node, "Main function not found");
        }

        SymTableEntry s = st.lookup("function_main");

        if (!s.getType().getDescriptor().contains("V")) {
            ASTUtils.error(node, "Return type must be void->function main");
        }

        if (s.getNo_parameters() != 0) {
            ASTUtils.error(node, "Main function expects zero parameters");
        }

        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {

        node.getExpression1().accept(this);
        Type t1 = ASTUtils.getSafeType(node.getExpression1());

        node.getExpression2().accept(this);
        Type t2 = ASTUtils.getSafeType(node.getExpression2());

        if (!TypeUtils.isAssignable(t1, t2)) {
            ASTUtils.error(node, "Expressions are not compatible in assignment");
        }
        ASTUtils.setType(node, t2);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        if (node.getStatements() != null) {
            for (Statement st : node.getStatements()) {
                st.accept(this);
            }
        }

        if (node.getCs() != null) {
            node.getCs().accept(this);
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        Type t1 = ASTUtils.getSafeType(node.getExpression1());

        node.getExpression2().accept(this);
        Type t2 = ASTUtils.getSafeType(node.getExpression2());

        if (node.getOperator().isRelational() && !t1.getDescriptor().contains("D")) {
            ASTUtils.error(node, "Relational operators can only be applied between numbers");
        } else if (node.getOperator().isRelational() && !t2.getDescriptor().contains("D")) {
            ASTUtils.error(node, "Relational operators can only be applied between numbers");
        }

        if (t1.getDescriptor().contains("D") && t2.getDescriptor().contains("D")) {
            if (node.getOperator().isRelational()) {
                ASTUtils.setType(node, Type.BOOLEAN_TYPE);
            } else {
                try {
                    ASTUtils.setType(node, TypeUtils.applyBinary(node.getOperator(), t1, t2));
                    ASTUtils.setType(node, Type.DOUBLE_TYPE);
                } catch (TypeException e) {
                    ASTUtils.error(node, e.getMessage());
                }
            }
        } else if (t1.getDescriptor().contains("Ljava/lang/String;") && t2.getDescriptor().contains("Ljava/lang/String;")) {
            try {
                ASTUtils.setType(node, TypeUtils.applyBinary(node.getOperator(), t1, t2));
                ASTUtils.setType(node, Type.CHAR_TYPE);
            } catch (TypeException e) {
                ASTUtils.error(node, e.getMessage());
            }
        } else if (node.getOperator().toString().contains("&&") || node.getOperator().toString().contains("||") || node.getOperator().toString().contains("!")) {
            if (!ASTUtils.getSafeType(node.getExpression1()).toString().contains("Z") || !ASTUtils.getSafeType(node.getExpression2()).toString().contains("Z")) {
                ASTUtils.error(node, "Boolean operators can only be applied between boolean expressions");
            }
            ASTUtils.setType(node, t1);
        } else {
            ASTUtils.error(node, "Operations only valid between numbers or strings");
        }
    }

    @Override
    public void visit(UnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        try {
            ASTUtils.setType(node, TypeUtils.applyUnary(node.getOperator(), ASTUtils.getSafeType(node.getExpression())));
        } catch (TypeException e) {
            ASTUtils.error(node, e.getMessage());
        }
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        String id = node.getIdentifier();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookupOnlyInTop(id) == null) {
            if (st.lookup("parameter_" + id) != null) {
                SymTableEntry e = st.lookup("parameter_" + id);
                ASTUtils.setType(node, e.getType());
            } else {
                SymTable<SymTableEntry> st1 = (SymTable<SymTableEntry>) Registry.getInstance().getRoot().getProperty("global_scope");
                SymTableEntry e = st1.lookup("function_" + id);
                if (e == null) {
                    for (String s : st1.getKeys()) {
                        System.out.println(s);
                    }
                    e = st1.lookup(id);
                    if (e == null) {
                        SymTableEntry e1 = st.lookup(id);
                        if (e1 == null) {
                            ASTUtils.error(node, "Undefined reference " + id);
                        } else {
                            ASTUtils.setType(node, e1.getType());
                        }
                    } else {
                        ASTUtils.setType(node, e.getType());
                    }
                } else {
                    ASTUtils.setType(node, e.getType());
                }
            }
        } else {
            SymTableEntry s = st.lookupOnlyInTop(id);
            ASTUtils.setType(node, s.getType());
        }

    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.INT_TYPE);
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.DOUBLE_TYPE);
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.getType(String.class));
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        ASTUtils.setType(node, ASTUtils.getSafeType(node.getExpression()));
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) {
            ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
        }
        node.getStatement().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(DeclarationStatement node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookupOnlyInTop(id) == null) {
            ASTUtils.error(node, "Variable " + id + " undefined");
        }

        Type t = node.getTs();

        ASTUtils.setType(node, t);
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) {
            ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
        }

        if (node.getStatement1() != null) {
            node.getStatement1().accept(this);
        }

        if (node.getStatement2() != null) {
            node.getStatement2().accept(this);
        }

        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) {
            ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
        }
        node.getStatement().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ClassDefinition node) throws ASTVisitorException {
        if (node.getFofd() != null) {
            node.getFofd().accept(this);
        }

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }

        String id = node.getIdentifier();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookup("class_" + id) == null) {
            ASTUtils.error(node, "Class " + id + " undefined");
        }

        Type t = Type.getType("Lorg/hua/" + node.getIdentifier() + ";");

        ASTUtils.setType(node, t);
    }

    @Override
    public void visit(FieldOrFunctionDefinition node) throws ASTVisitorException {
        if (node.getField() != null) {
            node.getField().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(FieldDefinition node) throws ASTVisitorException {
        if (node.getFd() != null) {
            node.getFd().accept(this);
        }

        if (node.getFuncDef() != null) {
            node.getFuncDef().accept(this);
        }

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookup(id) == null) {
            ASTUtils.error(node, "Field " + id + " undefined");
        }
        Type idType = node.getTs();

        ASTUtils.setType(node, idType);

    }

    @Override
    public void visit(FunctionDefinition node) throws ASTVisitorException {
        if (node.getParameters() != null) {
            for (ParameterDeclaration pd : node.getParameters()) {
                pd.accept(this);
            }
        }
        if (node.getCs() != null) {
            node.getCs().accept(this);
        }
        if (node.getFd() != null) {
            node.getFd().accept(this);
        }

        if (node.getCd() != null) {
            node.getCd().accept(this);
        }

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookup("function_" + id) == null) {
            ASTUtils.error(node, "Undefined function " + id);
        }

        Type t = node.getTs();
        ASTUtils.setType(node, t);
    }

    @Override
    public void visit(ParameterDeclaration node) throws ASTVisitorException {

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookupOnlyInTop("parameter_" + id) == null) {
            ASTUtils.error(node, "Parameter " + id + " undefined");
        }

        Type t = node.getTs();
        ASTUtils.setType(node, t);
    }

    @Override
    public void visit(VoidLiteralExpression node) throws ASTVisitorException {
        node.accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
            SymTable<SymTableEntry> st = (SymTable<SymTableEntry>) Registry.getInstance().getRoot().getProperty("global_scope");

            for (String s : st.getKeys()) {
                if (s.contains("function")) {
                    if (Registry.getInstance().getRoot().getProperty(s + "_returns") != null) {
                    } else {
                        Registry.getInstance().getRoot().setProperty(s + "_returns", true);
                        Type t = st.lookup(s).getType();
                        if (!t.getDescriptor().contains(ASTUtils.getSafeType(node.getE()).getDescriptor())) {
//                            ASTUtils.error(node, "Invalid Return type");
                        }
                    }
                }
            }
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(LogicalNotExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        ASTUtils.setType(node, Type.BOOLEAN_TYPE);
    }

    @Override
    public void visit(DotExpression node) throws ASTVisitorException {
        for (Expression s : node.getExpressions()) {
            s.accept(this);
        }
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);
        String id = node.getIdentifier();
        SymTableEntry e = st.lookup(id);
        if (e == null) {
            ASTUtils.error(node, "Undefined reference " + id);
        }
        Type idType = e.getType();

        node.getExpression().accept(this);
        Type exprType = ASTUtils.getSafeType(node.getExpression());

        if (!TypeUtils.isAssignable(idType, exprType)) {
            ASTUtils.error(node, "Expressions are not compatible in assignment");
        }
        ASTUtils.setType(node, idType);
    }

    @Override
    public void visit(NewExpression node) throws ASTVisitorException {
        for (Expression s : node.getExpressions()) {
            s.accept(this);
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(IdentifierExpressionList node) throws ASTVisitorException {
        for (Expression s : node.getExpressions()) {
            s.accept(this);
        }

        String id = node.getIdentifier();
        SymTable<SymTableEntry> st = (SymTable<SymTableEntry>) Registry.getInstance().getRoot().getProperty("global_scope");
        SymTableEntry e = st.lookup("function_" + id);

        if (e == null) {
            ASTUtils.error(node, "Undefined reference " + id);
        }

        if (e.getNo_parameters() != node.getExpressions().size()) {
            ASTUtils.error(node, "Invalid number of parameters");
        }

        Type idType = e.getType();
        ASTUtils.setType(node, idType);
    }

    @Override
    public void visit(ThisExpression node) throws ASTVisitorException {
        SymTable<SymTableEntry> st = (SymTable<SymTableEntry>) Registry.getInstance().getRoot().getProperty("global_scope");
        boolean has_Class = false;
        for (String ss : st.getKeys()) {
            if (ss.contains("class")) {
                has_Class = true;
            }
        }
        if (!has_Class) {
            ASTUtils.error(node, "Invalid use of keyword \"this\"");
        }

        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(NullExpression node) throws ASTVisitorException {
        node.accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(VariousExpressions node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(PrintStatement node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
    }
}
