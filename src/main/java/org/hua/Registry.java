/**
 * This code is part of the lab exercises for the Compilers course at Harokopio
 * University of Athens, Dept. of Informatics and Telematics.
 */
package org.hua;

import java.util.Hashtable;
import java.util.Map;
import org.hua.ast.ASTNode;
import org.hua.symbol.SymTable;
import org.hua.symbol.SymTableEntry;
import org.objectweb.asm.Type;

/**
 * Global registry (Singleton pattern)
 */
public class Registry {

    ASTNode root;

    private Registry() {
        root = null;
    }

    private static class SingletonHolder {

        public static Registry instance = new Registry();
        public static Map<Type, SymTable<SymTableEntry>> dictionary = new Hashtable<Type, SymTable<SymTableEntry>>();
    }

    public static Registry getInstance() {
        return SingletonHolder.instance;
    }

    public SymTable getSymTable(Type type) {
        return SingletonHolder.dictionary.get(type);
    }

    boolean lookup(Type t) {
        if (SingletonHolder.dictionary.isEmpty()) {
            return false;
        }
        return SingletonHolder.dictionary.containsKey(t);
    }

    void put(Type t, SymTable<SymTableEntry> st) {
        SingletonHolder.dictionary.put(t, st);
    }

    SymTableEntry getEntry(Type t, String id) {
        return SingletonHolder.dictionary.get(t).lookup(id);
    }

    public ASTNode getRoot() {
        return root;
    }

    public static Map<Type, SymTable<SymTableEntry>> getDictionary() {
        return SingletonHolder.dictionary;
    }

    public void setRoot(ASTNode root) {
        this.root = root;
    }

}
