package org.hua;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hua.ast.ASTNode;
import org.hua.ast.ASTUtils;
import org.hua.ast.ASTVisitor;
import org.hua.ast.ASTVisitorException;
import org.hua.ast.AssignmentStatement;
import org.hua.ast.BinaryExpression;
import org.hua.ast.BreakStatement;
import org.hua.ast.ClassDefinition;
import org.hua.ast.CompilationUnit;
import org.hua.ast.CompoundStatement;
import org.hua.ast.ContinueStatement;
import org.hua.ast.DeclarationStatement;
import org.hua.ast.DotExpression;
import org.hua.ast.DoubleLiteralExpression;
import org.hua.ast.Expression;
import org.hua.ast.FieldDefinition;
import org.hua.ast.FieldOrFunctionDefinition;
import org.hua.ast.FunctionDefinition;
import org.hua.ast.IdentifierExpression;
import org.hua.ast.IdentifierExpressionList;
import org.hua.ast.IfElseStatement;
import org.hua.ast.IfStatement;
import org.hua.ast.IntegerLiteralExpression;
import org.hua.ast.LogicalNotExpression;
import org.hua.ast.NewExpression;
import org.hua.ast.NullExpression;
import org.hua.ast.Operator;
import org.hua.ast.ParameterDeclaration;
import org.hua.ast.ParenthesisExpression;
import org.hua.ast.PrintStatement;
import org.hua.ast.ReturnStatement;
import org.hua.ast.Statement;
import org.hua.ast.StringLiteralExpression;
import org.hua.ast.ThisExpression;
import org.hua.ast.UnaryExpression;
import org.hua.ast.VariousExpressions;
import org.hua.ast.VoidLiteralExpression;
import org.hua.ast.WhileStatement;
import org.hua.symbol.LocalIndexPool;
import org.hua.symbol.SymTable;
import org.hua.symbol.SymTableEntry;
import org.hua.types.TypeUtils;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

public class BytecodeGeneratorASTVisitor implements ASTVisitor {

    private ClassNode cn;
    private MethodNode mn;
    private String className;
    String tmpId;
    Type functionReturnType;

    public Type getFunctionReturnType() {
        return functionReturnType;
    }

    public void setFunctionReturnType(Type functionReturnType) {
        this.functionReturnType = functionReturnType;
    }

    public String getTmpId() {
        return tmpId;
    }

    public void setTmpId(String tmpId) {
        this.tmpId = tmpId;
    }

    public BytecodeGeneratorASTVisitor(String filename) {

        className = filename;

        // create class
        cn = new ClassNode();
        cn.access = Opcodes.ACC_PUBLIC;
        cn.version = Opcodes.V1_5;
        cn.name = filename;
        cn.sourceFile = filename + ".in";
        cn.superName = "java/lang/Object";

        // create constructor
        mn = new MethodNode(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
        mn.instructions.add(new VarInsnNode(Opcodes.ALOAD, 0));
        mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V"));
        mn.instructions.add(new InsnNode(Opcodes.RETURN));
        mn.maxLocals = 1;
        mn.maxStack = 1;
        cn.methods.add(mn);

        //create write function
        String functionDescriptor = "(";
        functionDescriptor += "Ljava/lang/String;";
        functionDescriptor += ")";
        functionDescriptor += "V";
        mn = new MethodNode(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "write", functionDescriptor, null, null);
        mn.instructions.add(new InsnNode(Opcodes.RETURN));
        mn.maxLocals = 256;
        mn.maxStack = 256;
        cn.methods.add(mn);
    }

    public String getClassName() {
        return className;
    }

    @Override
    public void visit(CompilationUnit node) throws ASTVisitorException {

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }

    }

    @Override
    public void visit(ClassDefinition node) throws ASTVisitorException {

        if (node.getFofd() != null) {
            node.getFofd().accept(this);
        }

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
    }

    @Override
    public void visit(FieldOrFunctionDefinition node) throws ASTVisitorException {
        if (node.getField() != null) {
            node.getField().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
    }

    @Override
    public void visit(FieldDefinition node) throws ASTVisitorException {
        FieldNode fn = new FieldNode(Opcodes.ACC_PUBLIC, node.getId(), node.getTs().getDescriptor(), null, null);
        cn.fields.add(fn);

        SymTable<SymTableEntry> symTable = ASTUtils.getSafeEnv(node);
        SymTableEntry id = symTable.lookup(node.getId());

        if (id.getType().getDescriptor().contains("D")) {
            mn.instructions.add(new LdcInsnNode((double) 0));
            mn.instructions.add(new FieldInsnNode(Opcodes.PUTFIELD, this.getClassName(), node.getId(), node.getTs().getDescriptor()));
        } else {
            mn.instructions.add(new LdcInsnNode(""));
            mn.instructions.add(new FieldInsnNode(Opcodes.PUTFIELD, this.getClassName(), node.getId(), node.getTs().getDescriptor()));
        }

        if (node.getFd() != null) {
            node.getFd().accept(this);
        }
        if (node.getFuncDef() != null) {
            node.getFuncDef().accept(this);
        }
    }

    @Override
    public void visit(DeclarationStatement node) throws ASTVisitorException {

        SymTable<SymTableEntry> symTable = ASTUtils.getSafeEnv(node);
        SymTableEntry id = symTable.lookupOnlyInTop(node.getId());

        if (id.getType().getDescriptor().contains("D")) {
            mn.instructions.add(new LdcInsnNode((double) 0));
            mn.instructions.add(new VarInsnNode(id.getType().getOpcode(Opcodes.ISTORE), id.getIndex()));
        } else if (id.getType().getDescriptor().contains("Ljava/lang/String;")) {
            mn.instructions.add(new LdcInsnNode(""));
            mn.instructions.add(new VarInsnNode(id.getType().getOpcode(Opcodes.ISTORE), id.getIndex()));
        }

        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

    }

    @Override
    public void visit(FunctionDefinition node) throws ASTVisitorException {

        Type ftype = ASTUtils.getSafeType(node);

        String functionDescriptor = "(";
        if (node.getParameters() != null) {
            for (ParameterDeclaration parDec : node.getParameters()) {
                parDec.accept(this);
                Type parType = ASTUtils.getType(parDec);
                functionDescriptor += parType.getDescriptor();
            }
        }
        functionDescriptor += ")";
        functionDescriptor += ftype.getDescriptor();

        mn = new MethodNode(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, node.getId(), functionDescriptor, null, null);

        this.setFunctionReturnType(node.getTs());

        if (node.getCs() != null) {
            node.getCs().accept(this);
        }

        if (ftype.getDescriptor().contains("V")) {
            mn.instructions.add(new InsnNode(Opcodes.RETURN));
        }

        mn.maxLocals = 256;
        mn.maxStack = 256;
        cn.methods.add(mn);

        if (node.getCd() != null) {
            node.getCd().accept(this);
        }

        if (node.getFd() != null) {
            node.getFd().accept(this);
        }
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
        }

        if (this.getFunctionReturnType().getDescriptor().contains("D")) {
            mn.instructions.add(new InsnNode(Opcodes.DRETURN));
        } else if (this.getFunctionReturnType().getDescriptor().contains("Ljava/lang/String;")) {
            mn.instructions.add(new InsnNode(Opcodes.ARETURN));
        } else if (this.getFunctionReturnType().getDescriptor().contains("V")) {
            mn.instructions.add(new InsnNode(Opcodes.RETURN));
        }
    }

    @Override
    public void visit(ParameterDeclaration node) throws ASTVisitorException {
        String id = node.getId();
        SymTable<SymTableEntry> env = ASTUtils.getSafeEnv(node);
        SymTableEntry element = env.lookup(id);
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        JumpInsnNode jmp = new JumpInsnNode(Opcodes.GOTO, null);
        mn.instructions.add(jmp);
        ASTUtils.getBreakList(node).add(jmp);
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        JumpInsnNode jmp = new JumpInsnNode(Opcodes.GOTO, null);
        mn.instructions.add(jmp);
        ASTUtils.getContinueList(node).add(jmp);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        List<JumpInsnNode> breakList = new ArrayList<JumpInsnNode>();
        List<JumpInsnNode> continueList = new ArrayList<JumpInsnNode>();
        Statement s = null, ps;
        if (node.getStatements() != null) {
            Iterator<Statement> it = node.getStatements().iterator();
            while (it.hasNext()) {
                ps = s;
                s = it.next();
                if (ps != null && !ASTUtils.getNextList(ps).isEmpty()) {
                    LabelNode labelNode = new LabelNode();
                    mn.instructions.add(labelNode);
                    backpatch(ASTUtils.getNextList(ps), labelNode);
                }
                s.accept(this);
                breakList.addAll(ASTUtils.getBreakList(s));
                continueList.addAll(ASTUtils.getContinueList(s));
            }
            if (s != null) {
                ASTUtils.setNextList(node, ASTUtils.getNextList(s));
            }
            ASTUtils.setBreakList(node, breakList);
            ASTUtils.setContinueList(node, continueList);
        }

        if (node.getCs() != null) {
            node.getCs().accept(this);
        }
    }

    @Override
    public void visit(PrintStatement node) throws ASTVisitorException {

        if (node.getExpression() != null) {
            Type type = ASTUtils.getSafeType(node.getExpression());
            mn.instructions.add(new FieldInsnNode(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
            node.getExpression().accept(this);
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(" + type + ")V"));
        }
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);

        node.getExpression().accept(this);

        LabelNode labelNode = new LabelNode();
        mn.instructions.add(labelNode);
        backpatch(ASTUtils.getTrueList(node.getExpression()), labelNode);

        node.getStatement().accept(this);

        ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
        ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement()));

        ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement()));
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);

        node.getExpression().accept(this);

        LabelNode stmt1StartLabelNode = new LabelNode();
        mn.instructions.add(stmt1StartLabelNode);
        node.getStatement1().accept(this);

        JumpInsnNode skipGoto = new JumpInsnNode(Opcodes.GOTO, null);
        mn.instructions.add(skipGoto);

        LabelNode stmt2StartLabelNode = new LabelNode();
        mn.instructions.add(stmt2StartLabelNode);
        node.getStatement2().accept(this);

        backpatch(ASTUtils.getTrueList(node.getExpression()), stmt1StartLabelNode);
        backpatch(ASTUtils.getFalseList(node.getExpression()), stmt2StartLabelNode);

        ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement1()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement2()));
        ASTUtils.getNextList(node).add(skipGoto);

        ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement1()));
        ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement2()));

        ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement1()));
        ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement2()));
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);
        LabelNode beginLabelNode = new LabelNode();
        mn.instructions.add(beginLabelNode);
        node.getExpression().accept(this);
        LabelNode trueLabelNode = new LabelNode();
        mn.instructions.add(trueLabelNode);
        backpatch(ASTUtils.getTrueList(node.getExpression()), trueLabelNode);
        node.getStatement().accept(this);
        backpatch(ASTUtils.getNextList(node.getStatement()), beginLabelNode);
        mn.instructions.add(new JumpInsnNode(Opcodes.GOTO, beginLabelNode));
        List<JumpInsnNode> nextList = new ArrayList<JumpInsnNode>();
        if (ASTUtils.getFalseList(node.getExpression()) != null) {
            nextList.addAll(ASTUtils.getFalseList(node.getExpression()));
        }
        if (ASTUtils.getBreakList(node.getStatement()) != null) {
            nextList.addAll(ASTUtils.getBreakList(node.getStatement()));
        }
        ASTUtils.setNextList(node, nextList);
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            JumpInsnNode i = new JumpInsnNode(Opcodes.GOTO, null);
            mn.instructions.add(i);
            if (node.getLiteral() != 0) {
                ASTUtils.getTrueList(node).add(i);
            } else {
                ASTUtils.getFalseList(node).add(i);
            }
        } else {
            Double d = Double.valueOf(node.getLiteral());
            mn.instructions.add(new LdcInsnNode(d));
        }
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            JumpInsnNode i = new JumpInsnNode(Opcodes.GOTO, null);
            mn.instructions.add(i);
            if (node.getLiteral() != 0) {
                ASTUtils.getTrueList(node).add(i);
            } else {
                ASTUtils.getFalseList(node).add(i);
            }
        } else {
            Double d = node.getLiteral();
            mn.instructions.add(new LdcInsnNode(d));
        }
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            JumpInsnNode i = new JumpInsnNode(Opcodes.GOTO, null);
            mn.instructions.add(i);
            if (ASTUtils.isBooleanExpression(node)) {
                ASTUtils.error(node, "String used as boolean expression");
            }
        } else {
            String s = node.getLiteral();
            mn.instructions.add(new LdcInsnNode(s));
        }
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);

        if (ASTUtils.isBooleanExpression(node)) {
            List<JumpInsnNode> trueList = new ArrayList<JumpInsnNode>();
            JumpInsnNode jmp;

            jmp = new JumpInsnNode(Opcodes.IFNE, null);
            mn.instructions.add(jmp);
            trueList.add(jmp);
            ASTUtils.setTrueList(node, trueList);
            List<JumpInsnNode> falseList = new ArrayList<JumpInsnNode>();
            jmp = new JumpInsnNode(Opcodes.GOTO, null);
            mn.instructions.add(jmp);
            falseList.add(jmp);
            ASTUtils.setFalseList(node, falseList);
        }
    }

    @Override
    public void visit(UnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);

        Type type = ASTUtils.getSafeType(node.getExpression());

        if (node.getOperator().equals(Operator.MINUS)) {
            mn.instructions.add(new InsnNode(type.getOpcode(Opcodes.INEG)));
        } else {
            ASTUtils.error(node, "Operator not recognized.");
        }
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        Type type1 = ASTUtils.getSafeType(node.getExpression1());
        SymTable<SymTableEntry> st1 = (SymTable<SymTableEntry>) Registry.getInstance().getRoot().getProperty("global_scope");
        SymTableEntry lookup = st1.lookup(this.getTmpId());

        if (lookup == null) {
            SymTable<SymTableEntry> safeEnv = ASTUtils.getSafeEnv(node);
            lookup = safeEnv.lookup(this.getTmpId());
            node.getExpression2().accept(this);
            Type type2 = ASTUtils.getSafeType(node.getExpression2());
            widen(lookup.getType(), type2);
            Integer index = lookup.getIndex();
            mn.instructions.add(new VarInsnNode(type1.getOpcode(Opcodes.ISTORE), index));
        } else {
            node.getExpression2().accept(this);
            Type type2 = ASTUtils.getSafeType(node.getExpression2());
            widen(lookup.getType(), type2);
            mn.instructions.add(new FieldInsnNode(Opcodes.PUTFIELD, this.getClassName(), lookup.getId(), lookup.getType().getDescriptor()));
        }
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        this.setTmpId(node.getIdentifier());
        SymTable<SymTableEntry> safeEnv = ASTUtils.getSafeEnv(node);
        SymTableEntry lookup = safeEnv.lookup(node.getIdentifier());
        if (lookup == null) {
            if (safeEnv.lookup("parameter_" + node.getIdentifier()) != null) {
                SymTableEntry e = safeEnv.lookup("parameter_" + node.getIdentifier());
                Type t = e.getType();
                Integer index = e.getIndex();
                mn.instructions.add(new VarInsnNode(t.getOpcode(Opcodes.ILOAD), index));
            } else {
                SymTable<SymTableEntry> st1 = (SymTable<SymTableEntry>) Registry.getInstance().getRoot().getProperty("global_scope");
                SymTableEntry e = st1.lookup("function_" + node.getIdentifier());
                if (e == null) {
                    ASTUtils.error(node, "Undefined reference " + node.getIdentifier());
                }
                String functionName = node.getIdentifier();
                Type t = e.getType();
                String functionDescriptor = new String("()");
                functionDescriptor += t.getDescriptor();
                mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, this.getClassName(), functionName, functionDescriptor));
            }
        } else {
            Type type = lookup.getType();
            Integer index = lookup.getIndex();
            mn.instructions.add(new VarInsnNode(type.getOpcode(Opcodes.ILOAD), index));
        }
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        Type expr1Type = ASTUtils.getSafeType(node.getExpression1());

        node.getExpression2().accept(this);
        Type expr2Type = ASTUtils.getSafeType(node.getExpression2());

        Type maxType = TypeUtils.maxType(expr1Type, expr2Type);

        // cast top of stack to max
        if (!maxType.equals(expr2Type)) {
            widen(maxType, expr2Type);
        }

        // cast second from top to max
        if (!maxType.equals(expr1Type)) {
            LocalIndexPool lip = ASTUtils.getSafeLocalIndexPool(node);
            int localIndex = -1;
            if (expr2Type.equals(Type.DOUBLE_TYPE) || expr1Type.equals(Type.DOUBLE_TYPE)) {
                localIndex = lip.getLocalIndex(expr2Type);
                mn.instructions.add(new VarInsnNode(expr2Type.getOpcode(Opcodes.ISTORE), localIndex));
            } else {
                mn.instructions.add(new InsnNode(Opcodes.SWAP));
            }
            widen(maxType, expr1Type);
            if (expr2Type.equals(Type.DOUBLE_TYPE) || expr1Type.equals(Type.DOUBLE_TYPE)) {
                mn.instructions.add(new VarInsnNode(expr2Type.getOpcode(Opcodes.ILOAD), localIndex));
                lip.freeLocalIndex(localIndex, expr2Type);
            } else {
                mn.instructions.add(new InsnNode(Opcodes.SWAP));
            }
        }

        // 
        if (ASTUtils.isBooleanExpression(node)) {
            handleBooleanOperator(node, node.getOperator(), maxType);
        } else if (maxType.equals(TypeUtils.STRING_TYPE)) {
            mn.instructions.add(new InsnNode(Opcodes.SWAP));
            handleStringOperator(node, node.getOperator());
        } else {
            handleNumberOperator(node, node.getOperator(), maxType);
        }
    }

    @Override
    public void visit(VoidLiteralExpression node) throws ASTVisitorException {

    }

    @Override
    public void visit(LogicalNotExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(DotExpression node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

        if (node.getExpressions() != null) {
            if (!node.getExpressions().isEmpty()) {
                for (Expression e : node.getExpressions()) {
                    e.accept(this);
                }
            }
        }
    }

    @Override
    public void visit(NewExpression node) throws ASTVisitorException {
        for (Expression e : node.getExpressions()) {
            e.accept(this);
        }
    }

    @Override
    public void visit(IdentifierExpressionList node) throws ASTVisitorException {
        String functionDescriptor = new String("(");
        Type functionType = ASTUtils.getType(node);
        String functionName = node.getIdentifier();

        Type t;
        List<Expression> expressions = node.getExpressions();
        for (int i = 0; i < expressions.size(); i++) {
            expressions.get(i).accept(this);
            t = ASTUtils.getSafeType(expressions.get(i));
            functionDescriptor += t.getDescriptor();
        }
        functionDescriptor += ")";
        functionDescriptor += functionType.getDescriptor();
        mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, this.getClassName(), functionName, functionDescriptor));
    }

    @Override
    public void visit(ThisExpression node) throws ASTVisitorException {
    }

    @Override
    public void visit(NullExpression node) throws ASTVisitorException {
    }

    @Override
    public void visit(VariousExpressions node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    public ClassNode getClassNode() {
        return cn;
    }

    private void backpatch(List<JumpInsnNode> list, LabelNode labelNode) {
        if (list == null) {
            return;
        }
        for (JumpInsnNode instr : list) {
            instr.label = labelNode;
        }
    }

    /**
     * Cast the top of the stack to a particular type
     */
    private void widen(Type target, Type source) {
        if (source.equals(target)) {
            return;
        }

        if (source.equals(Type.BOOLEAN_TYPE)) {
            if (target.equals(Type.INT_TYPE)) {
                // nothing
            } else if (target.equals(Type.DOUBLE_TYPE)) {
                mn.instructions.add(new InsnNode(Opcodes.I2D));
            } else if (target.equals(TypeUtils.STRING_TYPE)) {
                mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Boolean", "toString", "(Z)Ljava/lang/String;"));
            }
        } else if (source.equals(Type.INT_TYPE)) {
            if (target.equals(Type.DOUBLE_TYPE)) {
                mn.instructions.add(new InsnNode(Opcodes.I2D));
            } else if (target.equals(TypeUtils.STRING_TYPE)) {
                mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Integer", "toString", "(I)Ljava/lang/String;"));
            }
        } else if (source.equals(Type.DOUBLE_TYPE)) {
            if (target.equals(TypeUtils.STRING_TYPE)) {
                mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Double", "toString", "(D)Ljava/lang/String;"));
            }
        }
    }

    private void handleBooleanOperator(Expression node, Operator op, Type type) throws ASTVisitorException {
        List<JumpInsnNode> trueList = new ArrayList<JumpInsnNode>();

        if (type.equals(TypeUtils.STRING_TYPE)) {
            mn.instructions.add(new InsnNode(Opcodes.SWAP));
            JumpInsnNode jmp = null;
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z"));
            switch (op) {
                case EQUAL:
                    jmp = new JumpInsnNode(Opcodes.IFNE, null);
                    break;
                case NOT_EQUAL:
                    jmp = new JumpInsnNode(Opcodes.IFEQ, null);
                    break;
                default:
                    ASTUtils.error(node, "Operator not supported on strings");
                    break;
            }
            mn.instructions.add(jmp);
            trueList.add(jmp);
        } else if (type.equals(Type.DOUBLE_TYPE)) {
            JumpInsnNode jmp = null;
            mn.instructions.add(new InsnNode(Opcodes.DCMPG));
            switch (op) {
                case GT:
                    jmp = new JumpInsnNode(Opcodes.IFGT, null);
                    break;
                case GET:
                    jmp = new JumpInsnNode(Opcodes.IFGE, null);
                    break;
                case LT:
                    jmp = new JumpInsnNode(Opcodes.IFLT, null);
                    break;
                case LET:
                    jmp = new JumpInsnNode(Opcodes.IFLE, null);
                    break;
                case EQUAL:
                    jmp = new JumpInsnNode(Opcodes.IFEQ, null);
                    break;
                case NOT_EQUAL:
                    jmp = new JumpInsnNode(Opcodes.IFNE, null);
                    break;
                default:
                    ASTUtils.error(node, "Operator not supported on strings");
                    break;
            }
            mn.instructions.add(jmp);
            trueList.add(jmp);
        } else {
            JumpInsnNode jmp = null;
            switch (op) {
                case EQUAL:
                    jmp = new JumpInsnNode(Opcodes.IF_ICMPEQ, null);
                    mn.instructions.add(jmp);
                    break;
                case NOT_EQUAL:
                    jmp = new JumpInsnNode(Opcodes.IF_ICMPNE, null);
                    mn.instructions.add(jmp);
                    break;
                case GT:
                    jmp = new JumpInsnNode(Opcodes.IF_ICMPGT, null);
                    mn.instructions.add(jmp);
                    break;
                case GET:
                    jmp = new JumpInsnNode(Opcodes.IF_ICMPGE, null);
                    mn.instructions.add(jmp);
                    break;
                case LT:
                    jmp = new JumpInsnNode(Opcodes.IF_ICMPLT, null);
                    mn.instructions.add(jmp);
                    break;
                case LET:
                    jmp = new JumpInsnNode(Opcodes.IF_ICMPLE, null);
                    mn.instructions.add(jmp);
                    break;
                default:
                    ASTUtils.error(node, "Operator not supported");
                    break;
            }
            trueList.add(jmp);
        }
        ASTUtils.setTrueList(node, trueList);
        List<JumpInsnNode> falseList = new ArrayList<JumpInsnNode>();
        JumpInsnNode jmp = new JumpInsnNode(Opcodes.GOTO, null);
        mn.instructions.add(jmp);
        falseList.add(jmp);
        ASTUtils.setFalseList(node, falseList);
    }

    /**
     * Assumes top of stack contains two strings
     */
    private void handleStringOperator(ASTNode node, Operator op) throws ASTVisitorException {
        if (op.equals(Operator.PLUS)) {
            mn.instructions.add(new TypeInsnNode(Opcodes.NEW, "java/lang/StringBuilder"));
            mn.instructions.add(new InsnNode(Opcodes.DUP));
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V"));
            mn.instructions.add(new InsnNode(Opcodes.SWAP));
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;"));
            mn.instructions.add(new InsnNode(Opcodes.SWAP));
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;"));
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;"));
        } else if (op.isRelational()) {
            LabelNode trueLabelNode = new LabelNode();
            switch (op) {
                case EQUAL:
                    mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z"));
                    mn.instructions.add(new JumpInsnNode(Opcodes.IFNE, trueLabelNode));
                    break;
                case NOT_EQUAL:
                    mn.instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z"));
                    mn.instructions.add(new JumpInsnNode(Opcodes.IFEQ, trueLabelNode));
                    break;
                default:
                    ASTUtils.error(node, "Operator not supported on strings");
                    break;
            }
            mn.instructions.add(new InsnNode(Opcodes.ICONST_0));
            LabelNode endLabelNode = new LabelNode();
            mn.instructions.add(new JumpInsnNode(Opcodes.GOTO, endLabelNode));
            mn.instructions.add(trueLabelNode);
            mn.instructions.add(new InsnNode(Opcodes.ICONST_1));
            mn.instructions.add(endLabelNode);
        } else {
            ASTUtils.error(node, "Operator not recognized");
        }
    }

    private void handleNumberOperator(ASTNode node, Operator op, Type type) throws ASTVisitorException {
        if (op.equals(Operator.PLUS)) {
            mn.instructions.add(new InsnNode(type.getOpcode(Opcodes.IADD)));
        } else if (op.equals(Operator.MINUS)) {
            mn.instructions.add(new InsnNode(type.getOpcode(Opcodes.ISUB)));
        } else if (op.equals(Operator.MULTIPLY)) {
            mn.instructions.add(new InsnNode(type.getOpcode(Opcodes.IMUL)));
        } else if (op.equals(Operator.DIVISION)) {
            mn.instructions.add(new InsnNode(type.getOpcode(Opcodes.IDIV)));
        } else if (op.isRelational()) {
            if (type.equals(Type.DOUBLE_TYPE)) {
                mn.instructions.add(new InsnNode(Opcodes.DCMPG));
                JumpInsnNode jmp = null;
                switch (op) {
                    case EQUAL:
                        jmp = new JumpInsnNode(Opcodes.IFEQ, null);
                        mn.instructions.add(jmp);
                        break;
                    case NOT_EQUAL:
                        jmp = new JumpInsnNode(Opcodes.IFNE, null);
                        mn.instructions.add(jmp);
                        break;
                    case GT:
                        jmp = new JumpInsnNode(Opcodes.IFGT, null);
                        mn.instructions.add(jmp);
                        break;
                    case GET:
                        jmp = new JumpInsnNode(Opcodes.IFGE, null);
                        mn.instructions.add(jmp);
                        break;
                    case LT:
                        jmp = new JumpInsnNode(Opcodes.IFLT, null);
                        mn.instructions.add(jmp);
                        break;
                    case LET:
                        jmp = new JumpInsnNode(Opcodes.IFLE, null);
                        mn.instructions.add(jmp);
                        break;
                    default:
                        ASTUtils.error(node, "Operator not supported");
                        break;
                }
                mn.instructions.add(new InsnNode(Opcodes.ICONST_0));
                LabelNode endLabelNode = new LabelNode();
                mn.instructions.add(new JumpInsnNode(Opcodes.GOTO, endLabelNode));
                LabelNode trueLabelNode = new LabelNode();
                jmp.label = trueLabelNode;
                mn.instructions.add(trueLabelNode);
                mn.instructions.add(new InsnNode(Opcodes.ICONST_1));
                mn.instructions.add(endLabelNode);
            } else if (type.equals(Type.INT_TYPE)) {
                LabelNode trueLabelNode = new LabelNode();
                switch (op) {
                    case EQUAL:
                        mn.instructions.add(new JumpInsnNode(Opcodes.IF_ICMPEQ, trueLabelNode));
                        break;
                    case NOT_EQUAL:
                        mn.instructions.add(new JumpInsnNode(Opcodes.IF_ICMPNE, trueLabelNode));
                        break;
                    case GT:
                        mn.instructions.add(new JumpInsnNode(Opcodes.IF_ICMPGT, trueLabelNode));
                        break;
                    case GET:
                        mn.instructions.add(new JumpInsnNode(Opcodes.IF_ICMPGE, trueLabelNode));
                        break;
                    case LT:
                        mn.instructions.add(new JumpInsnNode(Opcodes.IF_ICMPLT, trueLabelNode));
                        break;
                    case LET:
                        mn.instructions.add(new JumpInsnNode(Opcodes.IF_ICMPLE, trueLabelNode));
                        break;
                    default:
                        break;
                }
                mn.instructions.add(new InsnNode(Opcodes.ICONST_0));
                LabelNode endLabelNode = new LabelNode();
                mn.instructions.add(new JumpInsnNode(Opcodes.GOTO, endLabelNode));
                mn.instructions.add(trueLabelNode);
                mn.instructions.add(new InsnNode(Opcodes.ICONST_1));
                mn.instructions.add(endLabelNode);
            } else {
                ASTUtils.error(node, "Cannot compare such types.");
            }
        } else {
            ASTUtils.error(node, "Operator not recognized.");
        }
    }
}
