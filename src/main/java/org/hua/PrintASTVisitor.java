package org.hua;

/**
 * This code is part of the lab exercises for the Compilers course at Harokopio
 * University of Athens, Dept. of Informatics and Telematics.
 */
import org.apache.commons.lang3.StringEscapeUtils;
import org.hua.ast.ASTVisitor;
import org.hua.ast.ASTVisitorException;
import org.hua.ast.AssignmentStatement;
import org.hua.ast.BinaryExpression;
import org.hua.ast.BreakStatement;
import org.hua.ast.ClassDefinition;
import org.hua.ast.CompilationUnit;
import org.hua.ast.CompoundStatement;
import org.hua.ast.ContinueStatement;
import org.hua.ast.DeclarationStatement;
import org.hua.ast.DotExpression;
import org.hua.ast.DoubleLiteralExpression;
import org.hua.ast.Expression;
import org.hua.ast.FieldDefinition;
import org.hua.ast.FieldOrFunctionDefinition;
import org.hua.ast.FunctionDefinition;
import org.hua.ast.IdentifierExpression;
import org.hua.ast.IdentifierExpressionList;
import org.hua.ast.IfElseStatement;
import org.hua.ast.IfStatement;
import org.hua.ast.IntegerLiteralExpression;
import org.hua.ast.LogicalNotExpression;
import org.hua.ast.NewExpression;
import org.hua.ast.NullExpression;
import org.hua.ast.ParameterDeclaration;
import org.hua.ast.ParenthesisExpression;
import org.hua.ast.PrintStatement;
import org.hua.ast.ReturnStatement;
import org.hua.ast.Statement;
import org.hua.ast.StringLiteralExpression;
import org.hua.ast.ThisExpression;
import org.hua.ast.UnaryExpression;
import org.hua.ast.VariousExpressions;
import org.hua.ast.VoidLiteralExpression;
import org.hua.ast.WhileStatement;

public class PrintASTVisitor implements ASTVisitor {

    @Override
    public void visit(CompilationUnit node) throws ASTVisitorException {
        if (node.getMyClass() != null) {
            ClassDefinition classDefinition = node.getMyClass();
            classDefinition.accept(this);
        }

        if (node.getMyFunction() != null) {
            FunctionDefinition cunctionDefinition = node.getMyFunction();
            cunctionDefinition.accept(this);
        }
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        if (node.getExpression1() != null) {
            node.getExpression1().accept(this);
        }
        System.out.print(" = ");
        if (node.getExpression2() != null) {
            node.getExpression2().accept(this);
        }
        System.out.println(";");
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);

        System.out.print(" ");
        System.out.print(node.getOperator().getType());
        System.out.print(" ");

        node.getExpression2().accept(this);
    }

    @Override
    public void visit(UnaryExpression node) throws ASTVisitorException {
        System.out.print(node.getOperator().getType());
        System.out.print(" ");

        node.getExpression().accept(this);
    }

    @Override
    public void visit(LogicalNotExpression node) throws ASTVisitorException {
        System.out.print(node.getOperator().getType());
        System.out.print(" ");

        node.getExpression().accept(this);
    }

    @Override
    public void visit(NullExpression node) throws ASTVisitorException {
        System.out.print(" null ");
    }

    @Override
    public void visit(ThisExpression node) throws ASTVisitorException {
        System.out.print(" this ");
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        System.out.print(node.getIdentifier() + " ");
    }

    @Override
    public void visit(IdentifierExpressionList node) throws ASTVisitorException {
        System.out.print("Call to function: " + node.getIdentifier() + " ");

        if (node.getExpressions() != null) {
            if (!node.getExpressions().isEmpty()) {
                for (Expression e : node.getExpressions()) {
                    e.accept(this);
                }
            }
        }
    }

    @Override
    public void visit(DotExpression node) throws ASTVisitorException {
        System.out.print(node.getIdentifier());

        node.getExpression().accept(this);

        if (node.getExpressions() != null) {
            if (!node.getExpressions().isEmpty()) {
                for (Expression e : node.getExpressions()) {
                    e.accept(this);
                }
            }
        }
        System.out.println("\n");
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        System.out.print(node.getLiteral());
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        System.out.print(node.getLiteral());
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        System.out.print("\"");
        System.out.print(StringEscapeUtils.escapeJava(node.getLiteral()));
        System.out.print("\"");
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        System.out.print("( ");
        node.getExpression().accept(this);
        System.out.print(" )");
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        if (node.getStatements() != null) {
            for (Statement s : node.getStatements()) {
                s.accept(this);
            }
        }
    }

    public void visit(WhileStatement node) throws ASTVisitorException {
        System.out.print("while(");

        Expression expression = node.getExpression();
        expression.accept(this);
        System.out.print(")");
        System.out.println("");
        Statement statement = node.getStatement();
        statement.accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        System.out.print("if( ");
        node.getExpression().accept(this);
        System.out.println(")");
        node.getStatement().accept(this);
        System.out.println("");
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        System.out.print("if( ");
        node.getExpression().accept(this);
        System.out.println(")");
        node.getStatement1().accept(this);
        System.out.println("\nelse");
        node.getStatement2().accept(this);
        System.out.println("");
    }

    @Override
    public void visit(FieldDefinition node) throws ASTVisitorException {
        System.out.println("identifier: " + node.getId() + " type: " + node.getTs().getDescriptor());
        if (node.getFd() != null) {
            node.getFd().accept(this);
        }
        if (node.getFuncDef() != null) {
            node.getFuncDef().accept(this);
        }
    }

    @Override
    public void visit(ClassDefinition node) throws ASTVisitorException {
        System.out.println("class: " + node.getIdentifier());
        if (node.getFofd() != null) {
            node.getFofd().accept(this);
        }
        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }

    }

    @Override
    public void visit(FieldOrFunctionDefinition node) throws ASTVisitorException {
        if (node.getField() != null) {
            node.getField().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
    }

    @Override
    public void visit(FunctionDefinition node) throws ASTVisitorException {
        System.out.println("Function: " + node.getId() + " Return Type: " + node.getTs().getDescriptor());
        if (node.getParameters() != null) {
            for (ParameterDeclaration pd : node.getParameters()) {
                pd.accept(this);
            }
        }
        if (node.getFd() != null) {
            node.getFd().accept(this);
        }
        if (node.getCs() != null) {
            node.getCs().accept(this);
        }

        if (node.getCd() != null) {
            node.getCd().accept(this);
        }
    }

    @Override
    public void visit(ParameterDeclaration node) throws ASTVisitorException {
        System.out.println("Parameter: " + node.getId() + " type: " + node.getTs().getDescriptor());
    }

    @Override
    public void visit(VoidLiteralExpression node) throws ASTVisitorException {
        System.out.println("void ");
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        System.out.print("return ");
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        System.out.println("break;");
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        System.out.println("continue;");
    }

    @Override
    public void visit(NewExpression node) throws ASTVisitorException {
        for (Expression e : node.getExpressions()) {
            e.accept(this);
        }
    }

    @Override
    public void visit(DeclarationStatement node) throws ASTVisitorException {
        if (node.getId() != null) {
            System.out.println("identifier: " + node.getId() + " type: " + node.getTs());
        }

        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
    }

    @Override
    public void visit(VariousExpressions node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    @Override
    public void visit(PrintStatement node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            System.out.print("print(");
            node.getExpression().accept(this);
            System.out.println(");");
        }
    }
}
