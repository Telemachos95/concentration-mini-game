/**
 * This code is part of the lab exercises for the Compilers course at Harokopio
 * University of Athens, Dept. of Informatics and Telematics.
 */
package org.hua;

import org.hua.symbol.SymTable;
import org.hua.symbol.SymTableEntry;
import org.hua.ast.ASTUtils;
import org.hua.ast.ASTVisitor;
import org.hua.ast.ASTVisitorException;
import org.hua.ast.AssignmentStatement;
import org.hua.ast.BinaryExpression;
import org.hua.ast.BreakStatement;
import org.hua.ast.ClassDefinition;
import org.hua.ast.CompilationUnit;
import org.hua.ast.CompoundStatement;
import org.hua.ast.ContinueStatement;
import org.hua.ast.DoubleLiteralExpression;
import org.hua.ast.IdentifierExpression;
import org.hua.ast.IfElseStatement;
import org.hua.ast.IfStatement;
import org.hua.ast.IntegerLiteralExpression;
import org.hua.ast.ParenthesisExpression;
import org.hua.ast.StringLiteralExpression;
import org.hua.ast.UnaryExpression;
import org.hua.ast.DeclarationStatement;
import org.hua.ast.DotExpression;
import org.hua.ast.Expression;
import org.hua.ast.FieldDefinition;
import org.hua.ast.FieldOrFunctionDefinition;
import org.hua.ast.FunctionDefinition;
import org.hua.ast.IdentifierExpressionList;
import org.hua.ast.LogicalNotExpression;
import org.hua.ast.NewExpression;
import org.hua.ast.NullExpression;
import org.hua.ast.ParameterDeclaration;
import org.hua.ast.PrintStatement;
import org.hua.ast.ReturnStatement;
import org.hua.ast.Statement;
import org.hua.ast.ThisExpression;
import org.hua.ast.VariousExpressions;
import org.hua.ast.VoidLiteralExpression;
import org.hua.ast.WhileStatement;
import org.hua.symbol.LocalIndexPool;
import org.objectweb.asm.Type;

/**
 * Collect all symbols such as variables, methods, etc in symbol table.
 */
public class CollectSymbolsASTVisitor implements ASTVisitor {

    public CollectSymbolsASTVisitor() {
    }

    @Override
    public void visit(CompilationUnit node) throws ASTVisitorException {

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }
        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);
        Registry.getInstance().getRoot().setProperty("global_scope", st);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        if (node.getStatements() != null) {
            for (Statement st : node.getStatements()) {
                st.accept(this);
            }
        }

        if (node.getCs() != null) {
            node.getCs().accept(this);
        }
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(UnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        // nothing
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        // nothing        
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        // nothing
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        // nothing
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(DeclarationStatement node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookup(id) != null) {
            SymTableEntry s = st.lookup(id);
            if (s.getRole().contains("variable") || s.getRole().contains("parameter")) {
                ASTUtils.error(node, "Variable " + id + " redefined");
            }
        }

        LocalIndexPool safeLocalIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        Type t = node.getTs();
        int localIndex = safeLocalIndexPool.getLocalIndex(t);
        st.put(id, new SymTableEntry(id, t, "variable", localIndex));
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        if (Registry.getInstance().getRoot().getProperty("may_break") == null) {
            ASTUtils.error(node, "Break used without a loop");
            Registry.getInstance().getRoot().setProperty("may_break", null);
        }
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        if (Registry.getInstance().getRoot().getProperty("may_continue") == null) {
            ASTUtils.error(node, "Continue used without a loop");
            Registry.getInstance().getRoot().setProperty("may_continue", null);
        }
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        node.getStatement1().accept(this);
        node.getStatement2().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(ClassDefinition node) throws ASTVisitorException {
        if (node.getFofd() != null) {
            node.getFofd().accept(this);
        }

        if (node.getMyClass() != null) {
            node.getMyClass().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }

        String id = node.getIdentifier();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookup("class_" + id) != null) {
            ASTUtils.error(node, "Class " + id + " redefined");
        }

        LocalIndexPool safeLocalIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        Type t = Type.getType("Lorg/hua/" + node.getIdentifier() + ";");
        int localIndex = safeLocalIndexPool.getLocalIndex(t);
        st.put("class_" + id, new SymTableEntry(id, t, "class", localIndex));
    }

    @Override
    public void visit(FieldOrFunctionDefinition node) throws ASTVisitorException {

        if (node.getField() != null) {
            node.getField().accept(this);
        }

        if (node.getMyFunction() != null) {
            node.getMyFunction().accept(this);
        }
    }

    @Override
    public void visit(FieldDefinition node) throws ASTVisitorException {
        if (node.getFd() != null) {
            node.getFd().accept(this);
        }

        if (node.getFuncDef() != null) {
            node.getFuncDef().accept(this);
        }

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (st.lookup(id) != null) {
            SymTableEntry s = st.lookup(id);
            if (s.getRole().contains("field")) {
                ASTUtils.error(node, "Field " + id + " redefined");
            }
        }

        LocalIndexPool safeLocalIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        Type t = node.getTs();
        int localIndex = safeLocalIndexPool.getLocalIndex(t);
        st.put(id, new SymTableEntry(id, t, "field", localIndex));
    }

    @Override
    public void visit(FunctionDefinition node) throws ASTVisitorException {

        if (node.getCs() != null) {
            node.getCs().accept(this);
        }

        if (node.getParameters() != null) {
            for (ParameterDeclaration pd : node.getParameters()) {
                pd.accept(this);
            }
        }

        if (node.getFd() != null) {
            node.getFd().accept(this);
        }

        if (node.getCd() != null) {
            node.getCd().accept(this);
        }

        String id = node.getId();
        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);

        if (id.equals("write")) {
            ASTUtils.error(node, "Reserved function name write");
        }

        if (st.lookup("function_" + id) != null) {
            ASTUtils.error(node, "Function " + id + " redefined");
        }

        int counter = 0;
        if (node.getParameters() != null) {
            counter = node.getParameters().size();
        }

        LocalIndexPool safeLocalIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        Type t = node.getTs();
        int localIndex = safeLocalIndexPool.getLocalIndex(t);
        st.put("function_" + id, new SymTableEntry(id, t, "function", counter, localIndex));
    }

    @Override
    public void visit(ParameterDeclaration node) throws ASTVisitorException {
        String id = node.getId();
        Type t = node.getTs();

        SymTable<SymTableEntry> st = ASTUtils.getSafeEnv(node);
        if (st.lookupOnlyInTop("parameter_" + id) != null) {
            ASTUtils.error(node, "Parameter " + id + " redefined");
        }
        LocalIndexPool safeLocalIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        int localIndex = safeLocalIndexPool.getLocalIndex(t);
        st.put("parameter_" + id, new SymTableEntry(id, t, "parameter", localIndex));
    }

    @Override
    public void visit(VoidLiteralExpression node) throws ASTVisitorException {
        // nothing
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    @Override
    public void visit(LogicalNotExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(DotExpression node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }

        if (node.getExpressions() != null) {
            for (Expression e : node.getExpressions()) {
                e.accept(this);
            }
        }
    }

    @Override
    public void visit(NewExpression node) throws ASTVisitorException {
        if (node.getExpressions() != null) {
            if (!node.getExpressions().isEmpty()) {
                for (Expression e : node.getExpressions()) {
                    e.accept(this);
                }
            }
        }
    }

    @Override
    public void visit(IdentifierExpressionList node) throws ASTVisitorException {
        for (Expression e : node.getExpressions()) {
            e.accept(this);
        }
    }

    @Override
    public void visit(ThisExpression node) throws ASTVisitorException {
        // nothing
    }

    @Override
    public void visit(NullExpression node) throws ASTVisitorException {
        // nothing
    }

    @Override
    public void visit(VariousExpressions node) throws ASTVisitorException {
        if (node.getE() != null) {
            node.getE().accept(this);
        }
    }

    @Override
    public void visit(PrintStatement node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            node.getExpression().accept(this);
        }
    }

}
